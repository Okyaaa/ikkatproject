import * as React from 'react';
import {View, Text} from 'react-native';
import {AsyncStorage} from 'react-native';
import TabNavigation from './src/navigation/Navigation';
import Navigation from './src/navigation/Navigation';

export default function App() {
  return (
    <Navigation />
  );
}
