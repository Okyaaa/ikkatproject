// In App.js in a new project

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/Home';
import OnboardScreen from '../screens/Onboard';
import LoginScreen from '../screens/Login';
import SignUpScreen from '../screens/SignUp';
import Shop from '../screens/Shop';
import Transaction from '../screens/Transaction';
import Account from '../screens/Account';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DetailProduct from '../screens/DetailProduct';
import Wishlist from '../screens/Wishlist';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

// const Navigation = () => {
//   const [isFirstLaunch, setIsFirstLauch] = React.useState(null);

//   React.useEffect(() => {
//     AsyncStorage.getItem('alreadyLaunched').then(value => {
//       if (value == null) {
//         AsyncStorage.setItem('alreadyLaunched', 'true');
//         setIsFirstLauch(true);
//       } else {
//         setIsFirstLauch(false);
//       }
//     });
//   }, []);

//   return (
//     <NavigationContainer>
//       <Stack.Navigator initialRouteName={isFirstLaunch ? 'Login' : 'OnBoard'}>
//         <Stack.Screen
//           name="Onboard"
//           component={OnboardScreen}
//           options={{headerShown: false}}
//         />
//         <Stack.Screen
//           name="Login"
//           component={LoginScreen}
//           options={{headerShown: false}}
//         />
//         <Stack.Screen
//           name="SignUp"
//           component={SignUpScreen}
//           options={{headerShown: true}}
//         />
//         <Stack.Screen
//           name="Home"
//           component={HomeScreen}
//           navigationop
//           options={{headerShown: false}}
//         />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );

// if (isFirstLaunch == null) {
//   return null;
// } else if (isFirstLaunch == true) {
//   return (

//   );
// } else {
//   return <LoginScreen />;
// }
// };

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Onboard"
          component={OnboardScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUpScreen}
          options={{headerShown: true}}
        />
        <Stack.Screen
          name="Home"
          component={TabNavigation}
          navigationop
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DetailProduct"
          component={DetailProduct}
          options={{headerShown: true}}
        />
        <Stack.Screen
          name="Wishlist"
          component={Wishlist}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;

export function TabNavigation () {
  return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home';
            } else if (route.name === 'Shop') {
              iconName = focused ? 'shopping-cart' : 'shopping-cart';
            } else if (route.name === 'Transaction') {
              iconName = focused ? 'exchange-alt' : 'exchange-alt';
            } else if (route.name === 'Account') {
              iconName = focused ? 'user-alt' : 'user-alt';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
          headerShown: false,
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
        })}>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Shop" component={Shop} />
        <Tab.Screen name="Transaction" component={Transaction} />
        <Tab.Screen name="Account" component={Account} />
      </Tab.Navigator>
      /* <Tab.Navigator>
        <Tab.Screen name="Home"
          component={HomeScreen}
          options={{tabBarIcon: }}
        />
        <Tab.Screen name="Shop" component={Shop} />
        <Tab.Screen name="Transaction" component={Transaction} />
        <Tab.Screen name="Account" component={Account} />
      </Tab.Navigator> */
  );
};
