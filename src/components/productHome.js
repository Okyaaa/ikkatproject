import React, {Component, useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Image} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {COLORS, images, theme} from '../constants';
import Icon2 from 'react-native-vector-icons/FontAwesome';

const ProductHome = props => {
  const [defaultRating, setDefaultRating] = useState(3);
  const [maxRating, setMaxRating] = useState([0, 1, 2, 3, 4]);

  const {starOutline, starFilled} = images;

  const CustomRatingBar = () => {
    return (
      <View style={styles.customRatingBarStyle}>
        {maxRating.map((item, key) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              key={item}
              onPress={() => setDefaultRating(item)}>
              <Icon2
                style={styles.starStyle}
                name={item <= defaultRating ? 'star' : 'star-o'}
              />
              {/* <Image
                  style={styles.starImgStyle}
                  source={
                    item <= defaultRating
                      ? starFilled
                      : starOutline
                  }
                /> */}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <View style={styles.productWrapper}>
      <TouchableOpacity
        onPress={props.onPress}
        style={[styles.container, {backgroundColor: props.color}]}>
        <View style={{flexDirection: 'row', flexDwidth: 140, height: 180}}>
          <Image
            source={{uri: props.source}}
            style={styles.image}
          />
        </View>
        <View style={{marginLeft: 5}}>
          <Text style={styles.namaProduct}>{props.productName}</Text>
          <Text style={styles.priceProduct}>{props.basePrice}</Text>
          <CustomRatingBar />
          <View style={{flexDirection: 'row', alignItems: 'center', marginTop:10}}>
            <Icon2 style={styles.homeProduct} name="home" />
            <Text style={{fontSize: 10}}>{props.shopName} | {props.channelName}</Text>
          </View>
        </View>
      </TouchableOpacity>

      <Text style={styles.submitText}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({

  productWrapper: {
    backgroundColor: 'red',
    flexDirection: 'column', 
    marginLeft: 16,
    width: 240,
    height: 310,
    borderRadius: 10,
    // shadowColor: '#000000',
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1.0,
    elevation: 5,
  },

  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: "100%",
    height: "100%",
    // borderRadius: 10,
    // borderWidth: 0,
    borderRadius: 10
  },

  submitText: {
    fontSize: 14,
    color: '#222222',
    alignSelf: 'center',
    // marginVertical: 10,
  },

  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10
  },

  namaProduct: {
    marginTop: 5,
  },

  priceProduct: {
    marginTop: 20,
  },

  customRatingBarStyle: {
    // justifyContent: 'center',
    flexDirection: 'row',
  },

  starStyle: {
    marginTop: 5,
    width: 20,
    height: 20,
  },

  homeProduct: {
    paddingRight: 5,
  },
});

export default ProductHome;
