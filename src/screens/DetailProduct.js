import React, {Component, useState, useEffect} from 'react';
import {View, StyleSheet, Text, Dimensions, Image} from 'react-native';
import {BASE_URL, BASE_URL_PROD, XSignature} from '../config';
import axios from 'axios';
import {theme} from '../constants';
<<<<<<< HEAD
import {ScrollView, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Counter from '../components/Counter';
import {SearchBar, withTheme} from 'react-native-elements';
import Wishlist from './Wishlist';
=======
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Counter from '../components/Counter';
import {SearchBar, withTheme} from 'react-native-elements';
>>>>>>> ba6ab75f25326373a547fbe4c4271ee6d9f30031

const {SIZES} = theme;

function DetailProduct({route, navigation}) {
  const [productDetail, setProductDetail] = useState();
  const [quantity, setQuantity] = useState(1);
  const [disable, setDisable] = useState(false);
  const [savedId, setSavedId] = useState();

  const handleAdd = () => {
    if (quantity >= 0) {
      setDisable(false);
    }
    setQuantity(quantity + 1);
  };

  const handleMinus = () => {
    if (quantity <= 1) {
      setDisable(true);
    }
    setQuantity(quantity - 1);
  };

  const getDetailProduct = async () => {
    await axios
      .post(
        `${BASE_URL_PROD}/service/product/detail`,
        {
          source: 'WEB',
          sourceDesc: 'master.com',
          productId: route.params.params.productId,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-Signature': XSignature,
          },
        },
      )
      .then(response => {
        const data = response.data;
        console.log(data);
        setProductDetail(data);
      })
      .catch(e => {
        console.log(`Error = ${e.message}`);
      });
  };

  useEffect(() => {
    // console.clear();
    getDetailProduct();
  }, []);

<<<<<<< HEAD
  const onWishlist = () => {
    navigation.navigate('Wishlist')
  };

=======
>>>>>>> ba6ab75f25326373a547fbe4c4271ee6d9f30031
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView>
        {productDetail ? (
          <>
            <View style={styles.container}>
              <View>
                <Image
                  source={{uri: productDetail?.images[0]}}
                  resizeMode="contain"
                  style={{width: '100%', height: 400}}
                />
              </View>
              <View style={styles.productName}>
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>
                  {productDetail.name}
                </Text>
              </View>
              <View style={styles.productName}>
                <Text
                  style={{fontSize: 20, color: '#E53630', fontWeight: 'bold'}}>
                  Rp{productDetail.basePrice}
                </Text>
              </View>
            </View>
            <View style={{height: 3, backgroundColor: '#C4C4C4'}} />
            <View
              style={{
                marginLeft: 16,
                marginRight: 16,
                marginTop: 4,
                marginBottom: 16,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>
                  Penilaian
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 4,
                  }}>
                  <Icon style={{color: '#E53630'}} name="star" />
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: 'bold',
                      color: '#E53630',
                      paddingLeft: 4,
                    }}>
                    {productDetail.rating}
                  </Text>
                </View>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>Kondisi</Text>
                <Text
                  style={{fontSize: 18, fontWeight: 'bold', color: '#E53630'}}>
                  Baru
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>Stock</Text>
                <Text
                  style={{fontSize: 18, fontWeight: 'bold', color: '#E53630'}}>
                  1000
                </Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontWeight: 'bold', fontSize: 16}}>Terjual</Text>
                <Text
                  style={{fontSize: 18, fontWeight: 'bold', color: '#E53630'}}>
                  28
                </Text>
              </View>
            </View>
            <View style={{height: 3, backgroundColor: '#C4C4C4'}} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginLeft: 16,
                marginRight: 16,
                marginTop: 8,
                marginBottom: 8,
              }}>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>Jumlah</Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  width: 100,
                }}>
                <Counter name="plus" handleChange={handleAdd}></Counter>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '100%',
                    paddingLeft: 20,
                    paddingRight: 20,
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: 20, fontWeight: '600'}}>
                    {quantity}
                  </Text>
                </View>
                <Counter
                  handleChange={handleMinus}
                  name="minus"
                  disabled={disable}></Counter>
              </View>
            </View>
            <View style={styles.shadowBottomBar}>
              <View style={styles.bottomBar}>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 4,
                  }}>
<<<<<<< HEAD
                  <TouchableOpacity
                    style={{alignItems: 'center'}}
                    onPress={onWishlist}
                    >
=======
                  <TouchableOpacity style={{alignItems: 'center'}}>
>>>>>>> ba6ab75f25326373a547fbe4c4271ee6d9f30031
                    <Icon style={{color: '#E53630'}} name="cart" size={30} />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#E53630',
                        paddingLeft: 4,
                      }}>
                      Tambah ke Keranjang
                    </Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity
<<<<<<< HEAD
                  onPress={() => console.log("Beli sekarang")}
=======
                  // onPress={onSignInPressed}
>>>>>>> ba6ab75f25326373a547fbe4c4271ee6d9f30031
                  style={[
                    styles.containerButton,
                    {backgroundColor: '#E53630'},
                  ]}>
                  <Text style={styles.submitText}>BELI SEKARANG</Text>
                </TouchableOpacity>
              </View>
            </View>
          </>
        ) : (
          <Text>Loading...</Text>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 4,
    margin: 16,
  },
  productName: {
    marginTop: 16,
  },
  bottomBar: {
    marginTop: 16,
    marginLeft: 16,
    marginRight: 18,
    flexDirection: 'row',
    // backgroundColor: 'white',
    // backgroundColor: 'red',
    justifyContent: 'space-between',
  },
  containerButton: {
    width: 150,
    height: 40,
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 0,
    justifyContent: 'center',
  },
  submitText: {
    fontSize: 14,
    fontWeight: '400',
    color: 'white',
    alignSelf: 'center',
    letterSpacing: 1,
  },
  shadowBottomBar: {
    elevation: 2,
    height: 100,
    width: '100%',
    marginTop: 4,
    borderWidth: 0,
  },
});

export default DetailProduct;
