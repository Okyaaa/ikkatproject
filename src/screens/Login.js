import React, {useContext, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import FormInputs from '../components/FormInput';
import FormInput from '../components/FormInput';
import Submit from '../components/Submit';
import {SIZES} from '../constants';
// import { AuthContext } from '../context/AuthContext';
import {Button} from 'react-native-elements';
import {AuthProvider} from '../context/AuthContext';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {BASE_URL, REQRES} from '../config';
import Icon from 'react-native-vector-icons/FontAwesome5';

const LoginScreen = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  // const {isLoading, login} = useContext(AuthContext);

  const onSignInPressed = async () => {
    // console.log('go to Login');
    // axios
    //   .post(`${REQRES}/api/login`, {
    //     email,
    //     password,
    //   })
    //   .then(response => {
    //     console.log(response.status);
    //     navigation.navigate('Home');
    //   })
    //   .catch(e => {
    //     console.log(`login error ${e}`);
    //     alert('Input username atau password dengan benar');
    //   });
      navigation.navigate('Home');
  };

  const onSignUpPressed = () => {
    console.log('go to SignUp');
    navigation.navigate('SignUp');
  };

  return (
    <ScrollView
      style={{backgroundColor: 'white'}}
      contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
      {/* <Spinner visible={isLoading}></Spinner> */}
      <View style={[styles.container, {flex: 1}]}>
        <Image
          source={require('../assets/images/loginpage.jpg')}
          resizeMode="center"
          style={styles.image}
        />
        <Text style={styles.textTitle}>Selamat datang</Text>
        <Text style={styles.textBody}>
          Silahkan masuk menggunakan akun anda
        </Text>
        <View style={{marginTop: 20}} />

        <FormInput name="Email" icon="user" value={text => setEmail(text)} />
        <FormInput
          name="Password"
          icon="lock"
          pass={true}
          value={text => setPassword(text)}
        />
        <View style={{width: '90%'}}>
          <Text style={([styles.textBody], {alignSelf: 'flex-end'})}>
            Lupa password ?
          </Text>
        </View>

        <TouchableOpacity
          onPress={onSignInPressed}
          style={[styles.containerButton, {backgroundColor: "red"}]}>
          <Text style={styles.submitText}>Login</Text>
        </TouchableOpacity>

        <View style={{flexDirection: 'row', marginVertical: 5}}>
          <Text style={styles.textBody}>TIdak memiliki akun ? </Text>
          <TouchableOpacity onPress={onSignUpPressed}>
            <Text style={[styles.textBody, {color: 'blue'}]}>Daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 400,
    height: 250,
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 40,
    marginVertical: 10,
    fontWeight: 'bold',
  },
  textBody: {
    fontSize: 16,
    marginVertical: 10,
  },
  containerButton: {
    width: '90%',
    height: 50,
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 0,
  },
  submitText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
    marginVertical: 10,
  },
});

export default LoginScreen;
