export const REQRES = 'https://reqres.in';
export const BASE_URL = 'https://demo.commerce.simpool.id/mp-master-wscommon';
export const BASE_URL_PROD = 'https://demo.commerce.simpool.id/mp-master-wsproduct';
export const XSignature = 'NjdlMmNmNGRlNTAwYjlmYTE3OWFjZWM0OThkY2VlMjY0Yzc2NmQ0ZjE1MmY3NDM3YmVjNTZkN2UzN2Y5ZGNmNFkyVnVkQzUyWlhJdU1pNHdpa2thdDIwMDcxNlkyVnVkQzUyWlhJdU1pNHdtb2JpbGVZMlZ1ZEM1MlpYSXVNaTR3U2ltcG9vbCBNb2JpbGUgQXBwcw=='